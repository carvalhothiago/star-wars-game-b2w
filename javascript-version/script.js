var url = 'https://swapi.co/api/planets/';
var sufix = '?format=json';
var count = 0;

init();

function doAjax(url, callback, methot = 'GET') {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState === 4) {
            if (xhttp.status === 200) {
                var response = JSON.parse(this.responseText);
                callback(response);
            }
        }
    };
    xhttp.open(methot, url, true);
    xhttp.send();
}

function init() {
    document.getElementById('loading').style.display = '';
    doAjax(
        url + sufix,
        function(response){
            count = (response.count);
            fillPlanet(response.results[getRandomNumber(response.results.length) - 1]);
        }
    );
}

function getPlanet(){
    if(count > 0) {
        document.getElementById('loading').style.display = '';
        doAjax(
            url + getRandomNumber(count) + sufix,
            fillPlanet
        );
    }
}

function fillPlanet(planet) {
    document.getElementsByClassName('name')[0].textContent = planet.name;
    document.getElementsByClassName('population')[0].textContent = planet.population;
    document.getElementsByClassName('climate')[0].textContent = planet.climate;
    document.getElementsByClassName('terrain')[0].textContent = planet.terrain;
    document.getElementsByClassName('film-count')[0].textContent = planet.films.length;
    document.getElementById('loading').style.display = 'none';
}

function getRandomNumber(numberQuantity) {
    return Math.floor((Math.random() * numberQuantity) + 1);
}