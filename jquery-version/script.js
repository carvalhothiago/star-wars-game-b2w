var url = 'https://swapi.co/api/planets/';
var sufix = '?format=json';
var count = 0;

$(document).ready(function() {
    init();
    $("#next-button").click(function() {
        getPlanet();
    });
});

function init() {
    $('#loading').show();
    $.ajax({
        url: url + sufix,
        success: function (data){
            count = (data.count);
            fillPlanet(data.results[getRandomNumber(data.results.length) - 1]);
        },
        error: function (){
            console.log("deu erro");
        }
    });
}

function getPlanet(){
    if(count > 0) {
        
        $('#loading').show();
        $.ajax({
            url: url + getRandomNumber(count) + sufix,
            success: function (data){
                fillPlanet(data);
            }
        });
    }
}

function fillPlanet(planet) {
    $(".name").text(planet.name);
    $(".population").text(planet.population);
    $(".climate").text(planet.climate);
    $(".terrain").text(planet.terrain);
    $(".film-count").text(planet.films.length);
    
    $('#loading').hide();
}

function getRandomNumber(numberQuantity) {
    return Math.floor((Math.random() * numberQuantity) + 1);
}