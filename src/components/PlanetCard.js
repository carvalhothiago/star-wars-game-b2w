import React, { Component } from 'react';
import './PlanetCard.css';

class PlanetCard extends Component {
  render() {
    
    return (
      <div className="planet-card">
        <div className="title name">
            <h1> {this.props.planet.name} </h1>
        </div>
        <div className="info">
            <p>Population: <strong className="population">{this.props.planet.population}</strong></p>
            <p>Climate: <strong className="climate">{this.props.planet.climate}</strong></p>
            <p>Terrain: <strong className="terrain">{this.props.planet.terrain}</strong></p>
            <p className="film">Featured in <strong className="film-count">{this.props.planet.films.length}</strong> Films</p>
        </div>
      </div>
    );
  }
}

export default PlanetCard;
