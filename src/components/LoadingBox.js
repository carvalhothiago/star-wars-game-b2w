import React, { Component } from 'react';
import './LoadingBox.css';

class LoadingBox extends Component {
  render() {
    return (
        <div id="loading" className={(this.props.showLoading ? 'show' : 'hidden')}>
          <br /><br /><br />
        </div>
    );
  }
}

export default LoadingBox;