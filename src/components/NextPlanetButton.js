import React, { Component } from 'react';
import './NextPlanetButton.css';

class NextPlanetButton extends Component {
  render() {
    return (
        <button id="next-button" onClick={this.props.do} > next planet</button>
    );
  }
}

export default NextPlanetButton;