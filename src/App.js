import React, { Component } from 'react';
import PlanetCard from './components/PlanetCard';
import LoadingBox from './components/LoadingBox';
import NextPlanetButton from './components/NextPlanetButton';
import './App.css';

class App extends Component {

  constructor(props) {
		super(props);
		this.state = {
      count: 0,
      showLoading: true,
      currentPlanet: {
        name: "",
        population: "",
        climate: "",
        terrain: "",
        films: [],
      }
    };
    this.getNextPlanet.bind(this);
	}

  componentWillMount(){
    doAjax(
        "https://swapi.co/api/planets/?format=json",
        function(response) {
            let randomNum = getRandomNumber(response.results.length) - 1;
            this.setState({
              count: (response.count),
              currentPlanet: response.results[randomNum],
              showLoading: false
            });
        }.bind(this)
    );
  }

  render() {
    return (
      <div id="main">
        <LoadingBox showLoading={this.state.showLoading}/>

        <PlanetCard planet={this.state.currentPlanet} />
        <NextPlanetButton do={this.getNextPlanet.bind(this)} />
      </div>
    );
  }
  

  getNextPlanet() {
    this.setState({ showLoading: true });

    let randomNum = getRandomNumber(this.state.count);
    doAjax(
      "https://swapi.co/api/planets/"+ randomNum +"/?format=json",
      function(response) {
        this.setState({
          currentPlanet: response,
          showLoading: false
        });
      }.bind(this)
    );
  }

}

export default App;

function doAjax(url, callback, methot = 'GET') {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (xhttp.readyState === 4 && xhttp.status === 200) {
          var response = JSON.parse(this.responseText);
          callback(response);
      }
  };
  xhttp.open(methot, url, true);
  xhttp.send();
}

function getRandomNumber(numberQuantity) {
  return Math.floor((Math.random() * numberQuantity) + 1);
}