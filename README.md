# star-wars-game-b2w

Esse projeto é o desafio da equipe B2W para a vaga de Front-end

Para rodar o projeto, você precisará: Git & NodeJs

1. Primeiro baixe o projeto utilizando o comando abaixo:
```
git clone https://carvalhothiago@bitbucket.org/carvalhothiago/star-wars-game-b2w.git
```
2. Após baixar o projeto, entre na pasta
```
cd star-wars-game-b2w
```
3. Execute o seguinte comando para baixar as dependências do projeto:
```
npm install
```
4. E por fim, basta executar o seguinte comando para iniciar o projeto:
```
npm start
```


Obs.: Dentro do projeto tem outras duas versões do sistema, uma com javascript somente, e outra utilizando jquery.